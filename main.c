#include <stdio.h>
#include <stdbool.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/struct.h"
#include "db/new.h"
#include "db/delete.h"

#include "create.h"
#include "check_arch.h"
#include "update.h"
#include "call_make.h"
#include "add.h"
#include "tree.h"
#include "prompt_for_project_type.h"
#include "process_dir_queue.h"
#include "process_src_queue.h"
#include "process_hdr_queue.h"

int main(int n, const char* args)
{
	bool first_time;
	struct db* db = new_db(&first_time);
	if(first_time)
	{
		prompt_for_project_type(db);
		create();
	}
	check_arch(db);
	add(db);
	// tree(db); // for debugging:
	update(db);
	process_dir_queue(db);
	process_src_queue(db);
	process_hdr_queue(db);
	delete_db(db);
	if(!first_time)
	{
		call_make(n, args);
	}
	return 0;
}
