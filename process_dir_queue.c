#include <stdlib.h>
#include <linux/limits.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/reprocess_dir/foreach.h"
#include "db/reprocess_dir/append.h"
#include "db/directory/get_parent.h"
#include "db/directory/get_refcount.h"
#include "db/directory/dec_refcount.h"
#include "db/directory/destory.h"
#include "db/directory/subdir/foreach.h"
#include "db/directory/subdir/remove.h"
#include "db/directory/source/foreach.h"
#include "db/source/get_filename.h"
#include "db/super/get_path.h"

void rewrite_directory_makefile(struct db* db, size_t dir)
{
	ENTER;
	char dirpath[PATH_MAX];
	db_super_get_path(db, dir, dirpath);
	verpvs(dirpath);
	char dir_makefile_path[PATH_MAX];
	strcat(strcpy(dir_makefile_path, dirpath), "/.dir.mk");
	verpvs(dir_makefile_path);
	int fd = open(dir_makefile_path, O_CREAT | O_TRUNC | O_WRONLY, 0666);
	if(fd < 0) perror("open");
	verpv(fd);
	db_directory_subdir_foreach(db, dir, ({
		void callback(size_t subdir_ptr)
		{
			verpv(subdir_ptr);
			char subpath[FILENAME_MAX];
			db_super_get_path(db, subdir_ptr, subpath);
			verpvs(subpath);
			dprintf(fd, "include %s/.dir.mk\n", subpath);
		}
		callback;
	}));
	db_directory_source_foreach(db, dir, ({
		void callback(size_t rec_ptr, size_t source_ptr)
		{
			verpv(source_ptr);
			char filename[FILENAME_MAX];
			db_source_get_filename(db, source_ptr, filename);
			verpvs(filename);
			dprintf(fd, "include %s/.%s.mk\n", dirpath, filename);
		}
		callback;
	}));
	dprintf(fd, "%s/.dir.o:", dirpath);
	db_directory_subdir_foreach(db, dir, ({
		void callback(size_t subdir_ptr)
		{
			verpv(subdir_ptr);
			char subpath[FILENAME_MAX];
			db_super_get_path(db, subdir_ptr, subpath);
			verpvs(subpath);
			dprintf(fd, " %s/.dir.o", subpath);
		}
		callback;
	}));
	db_directory_source_foreach(db, dir, ({
		void callback(size_t rec_ptr, size_t source_ptr)
		{
			verpv(source_ptr);
			char filename[FILENAME_MAX];
			db_source_get_filename(db, source_ptr, filename);
			verpvs(filename);
			dprintf(fd, " %s/.%s.o", dirpath, filename);
		}
		callback;
	}));
	dprintf(fd, "\n");
	close(fd);
	EXIT;
}

void process_dir_queue(struct db* db)
{
	ENTER;
	for(bool again = true;again;)
	{
		again = false;
		db_reprocess_dir_foreach(db, ({
			void callback(size_t dir_ptr)
			{
				ENTER;
				verpv(dir_ptr);
				int refcount = db_directory_get_refcount(db, dir_ptr);
				verpv(refcount);
				// if refcount is nonzero:
				if(refcount)
				{
					// printf("Rewriting directory @%li\n", dir_ptr);
					// rewrite makefile
					rewrite_directory_makefile(db, dir_ptr);
				}
				else
				{
					// printf("Removing directory @%li\n", dir_ptr);
					// if not root dir:
					if(true) // whatever
					{
						// delete directory makefile, and .o file || true
						{
							// whatever,
						}
						size_t parent = db_directory_get_parent(db, dir_ptr);
						// remove header from parent's subdir list
						db_directory_subdir_remove(db, parent, dir_ptr);
						// decrement parent's refcount,
						db_directory_dec_refcount(db, parent);
						// add parent directory to directory queue
						db_reprocess_dir_append(db, parent);
						// delete directory record
						db_directory_destory(db, dir_ptr);
						again = true;
					}
				}
				// otherway, remove from queue (done by db_reprocess_dir_foreach)
				EXIT;
			}
			callback;
		}));
	}
	EXIT;
}






