#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>

#define DEBUG 0
#include "libarray.h"
#include "libdebug.h"

#include "defines.h"
#include "db/arch_mk/is_outdated.h"
#include "db/arch_mk/set_flags.h"
#include "db/arch_mk/set_time.h"
#include "get_mtime.h"

struct array read_flags()
{
	ENTER;
	struct array cflag_arguments = new_array(const char*);
	FILE* arch = fopen(ARCH_MK_FILENAME, "r");
	char* line = NULL;
	size_t line_len = 0;
	ssize_t nread;
	#define WHITESPACE " \n"
	while((nread = getline(&line, &line_len, arch)) != -1)
	{
		char* m = line;
		char* varname = strtok_r(NULL, WHITESPACE, &m);
		char* operator = strtok_r(NULL, WHITESPACE, &m);
		if(varname && operator)
		{
			if(!strcmp(varname, "CPPFLAGS"))
			{
				void f()
				{
					for(char* word;word = strtok_r(NULL, WHITESPACE, &m);)
					{
						int len = strlen(word) + 1;
						char* copy = memcpy(malloc(len), word, len);
						array_push_n(&cflag_arguments, &copy);
					}
				}
				if(!strcmp(operator, "=")) {
					array_free_elements(&cflag_arguments);
					array_clear(&cflag_arguments);
					f();
				} else if(!strcmp(operator, "+=")) {
					f();
				} else {
					printf("unknown operator\n");
					abort();
				}
			}
		}
	}
	free(line);
	fclose(arch);
	EXIT;
	return cflag_arguments;
}

void check_arch(struct db* db)
{
	ENTER;
	time_t mtime = get_mtime(ARCH_MK_FILENAME);
	verprintf("mtime == %lu\n", mtime);
	bool is_outdated = db_arch_mk_is_outdated(db, mtime);
	verprintf("is_outdated == %i\n", is_outdated);
	if(is_outdated)
	{
		struct array new_flags = read_flags();
		db_arch_mk_set_flags(db, &new_flags);
		db_arch_mk_set_time(db, mtime);
/*		printf("CPPFLAGS's %i arguments have been stored\n", new_flags.n);*/
		array_free_elements(&new_flags);
		array_delete(&new_flags);
	}
	EXIT;
}












