#include <stdlib.h>
#include <unistd.h>
#include <linux/limits.h>
#include <assert.h>

#include "libdebug.h"
#include "libfileavl.h"

#include "db/struct.h"
#include "db/tree_ids.h"
#include "db/super/find.h"
#include "db/super/get_path.h"
#include "db/source/get_mtime.h"
#include "db/directory/get_refcount.h"
#include "db/directory/source/foreach.h"
#include "db/directory/subdir/foreach.h"

void tree(struct db* db)
{
	ENTER;
	int i, depth = 0;
	void go(char* path, size_t dir)
	{
		int refcount = db_directory_get_refcount(db, dir);
		for(i = depth;i--;) printf("\t");
			printf("References to '%s': %i\n", path, refcount);
		for(i = depth;i--;) printf("\t"); printf("Subdirs in '%s':\n", path);
		db_directory_subdir_foreach(db, dir, ({
			void callback(size_t subdir_ptr)
			{
				char path[PATH_MAX];
				int len = db_super_get_path(db, subdir_ptr, path);
				for(i = depth;i--;) printf("\t"); printf("'%s/':\n", path);
				depth++;
				go(path, subdir_ptr);
				depth--;
			}
			callback;
		}));
		for(i = depth;i--;) printf("\t"); printf("Source files in '%s':\n", path);
		db_directory_source_foreach(db, dir, ({
			void callback(size_t rec_ptr, size_t file_ptr)
			{
				char path[PATH_MAX];
				int len = db_super_get_path(db, file_ptr, path);
				time_t mtime = db_source_get_mtime(db, file_ptr);
				for(i = depth;i--;) printf("\t");
					printf("@%6lu: '%s', %lu\n", file_ptr, path, mtime);
			}
			callback;
		}));
	}
	size_t dir = db_super_find(db, ti_directories, ".");
	assert(dir);
	go(".", dir);
	CHECK;
	EXIT;
}







