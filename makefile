MAKEFLAGS += --no-print-directory

.PHONY: default
.PHONY: installdeps
.PHONY: install
.PHONY: both

default: main

both: installdeps main

include arch.mk
include install.mk
include run.mk
include .dir.mk

deps += ../libarray
deps += ../libfileavl
deps += ../libexec
deps += ../libdebug

deps_a = $(foreach dep,$(deps),$(dep).a)

.%.c.o: %.c
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@ || (gedit $< && false)
.%.cpp.o: %.cpp
	$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) $< -o $@
.dir.o:
	$(LD) -r $^ -o $@
%/.dir.o:
	$(LD) -r $^ -o $@

main: $(deps_a) .dir.o
	$(CC) $(LDFLAGS) .dir.o $(LOADLIBES) $(LDLIBS) $(deps_a) -o main

installdeps: cmd = $(MAKE) -C $(dep) install &&
installdeps:
	$(foreach dep,$(deps),$(cmd)) true

clean:
	find . -type f -name '*.o' | xargs rm -vf main
