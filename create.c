#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "./db/project_type/type.h"
#include "./db/project_type/get.h"
#include "./defines.h"
#include "./defaults/arch.mk.h"
#include "./defaults/gitignore.h"
#include "./defaults/gitlab-ci.yml.h"
#include "./defaults/for-app/makefile.h"
#include "./defaults/for-app/run.mk.h"
#include "./defaults/for-app/main.c.h"
#include "./defaults/for-app/install.mk.h"
#include "./defaults/for-lib/makefile.h"
#include "./defaults/for-lib/install.mk.h"
#include "./defaults/for-lib/public.h.h"

void write_to_file(const char* file, const char* content)
{
	int fd = open(file, O_WRONLY | O_EXCL | O_CREAT, 0666);
	if(fd < 0)
	{
		if(errno != EEXIST)
		{
			perror("creat");
		}
	}
	else
	{
		write(fd, content, strlen(content));
		close(fd);
	}
}

void create(struct db* db)
{
	write_to_file(ARCH_MK_FILENAME, arch_mk_content);
	write_to_file(GITIGNORE_FILENAME, gitignore_content);
	write_to_file(GITLAB_CI_YML_FILENAME, gitlab_ci_yml_content);
	switch(db_project_type_get(db))
	{
		case pt_application:
		{
			write_to_file(MAKEFILE_FILENAME, for_app_makefile_content);
			write_to_file(RUN_MK_FILENAME, for_app_run_mk_content);
			write_to_file(INSTALL_MK_FILENAME, for_app_install_mk_content);
			write_to_file(MAIN_C_FILENAME, for_app_main_c_content);
			break;
		}
		case pt_library:
		{
			write_to_file(MAKEFILE_FILENAME, for_lib_makefile_content);
			write_to_file(INSTALL_MK_FILENAME, for_lib_install_mk_content);
			write_to_file(PUBLIC_H_FILENAME, for_lib_public_h_content);
			break;
		}
		default: abort();
	}
}















