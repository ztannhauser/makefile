#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#define DEBUG 0
#include "libdebug.h"

void call_make(int n, const char** args)
{
	ENTER;
	args[0] = "make";
	execvp(args[0], args), perror("execv");
	EXIT;
}
