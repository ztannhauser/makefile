#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

time_t get_mtime(const char* path)
{
	time_t ret;
	struct stat sb;
	int stat_ret = stat(path, &sb);
	if(stat_ret < 0)
	{
		ret = 0;
	}
	else
	{
		ret = sb.st_mtim.tv_sec;
	}
	return ret;
}

