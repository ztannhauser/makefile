#include <stdlib.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/tree_ids.h"
#include "db/header/get_refcount.h"
#include "db/header/remove.h"
#include "db/reprocess_hdr/foreach.h"

void process_hdr_queue(struct db* db)
{
	ENTER;
	db_reprocess_hdr_foreach(db, ({
		void callback(size_t hdr_ptr)
		{
			ENTER;
			verpv(hdr_ptr);
			int refcount = db_header_get_refcount(db, hdr_ptr);
			verpv(refcount);
			// if refcount is nonzero:
			if(refcount == 0)
			{
				// delete header rec
				db_header_remove(db, hdr_ptr);
			}
			// otherway, remove from queue (done by db_reprocess_hdr_foreach)
			EXIT;
		}
		callback;
	}));
	EXIT;
}
