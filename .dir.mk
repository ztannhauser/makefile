include ./db/.dir.mk
include ./defaults/.dir.mk
include ./.prompt_for_project_type.c.mk
include ./.process_hdr_queue.c.mk
include ./.add.c.mk
include ./.check_arch.c.mk
include ./.call_make.c.mk
include ./.process_src_queue.c.mk
include ./.create.c.mk
include ./.tree.c.mk
include ./.get_mtime.c.mk
include ./.update.c.mk
include ./.process_dir_queue.c.mk
include ./.main.c.mk
./.dir.o: ./db/.dir.o ./defaults/.dir.o ./.prompt_for_project_type.c.o ./.process_hdr_queue.c.o ./.add.c.o ./.check_arch.c.o ./.call_make.c.o ./.process_src_queue.c.o ./.create.c.o ./.tree.c.o ./.get_mtime.c.o ./.update.c.o ./.process_dir_queue.c.o ./.main.c.o
