#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/struct.h"

#include "./struct.h"

time_t db_header_get_mtime(struct db* db, size_t header_ptr)
{
	ENTER;
	verpv(header_ptr);
	time_t mtime;
	pread(db->trees.fd, &mtime, sizeof(time_t),
		header_ptr + 
		sizeof(struct node_header) + 
		offsetof(struct header, mtime));
	verpv(mtime);
	EXIT;
	return mtime;
}
