#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "db/struct.h"

#include "./struct.h"

void db_header_set_mtime(struct db* db, size_t header_ptr, time_t mtime)
{
	ENTER;
	pwrite(db->trees.fd, &mtime, sizeof(time_t),
		header_ptr + 
		sizeof(struct node_header) + 
		offsetof(struct header, mtime));
	EXIT;
}
