
#include "db/super/struct.h"

struct header
{
	struct super super;
	int refcount;
	time_t mtime;
} __attribute__((packed)); 
