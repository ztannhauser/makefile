#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/tree_ids.h"
#include "db/struct.h"

#include "./struct.h"

int db_header_get_refcount(struct db* db, size_t header_ptr)
{
	ENTER;
	int refcount;
	pread(db->trees.fd, &refcount, sizeof(int),
		header_ptr + 
		sizeof(struct node_header) + 
		offsetof(struct header, refcount));
	EXIT;
	return refcount;
}
