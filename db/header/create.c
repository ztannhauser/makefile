#include <stdlib.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "db/tree_ids.h"
#include "db/super/create.h"

#include "./struct.h"

size_t db_header_create(struct db* db, char* path, time_t mtime, int refcount)
{
	ENTER;
	struct header hdr;
	db_super_create(&hdr, path);
	hdr.mtime = mtime;
	hdr.refcount = refcount;
	size_t ptr = fileavl_insert(db,
		ti_headers, &hdr, sizeof(struct header));
	verpv(ptr);
	EXIT;
	return ptr;
}
