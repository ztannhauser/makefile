#include <stdlib.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "db/tree_ids.h"

void db_header_remove(struct db* db, size_t header_ptr)
{
	ENTER;
	fileavl_remove(db, ti_headers, header_ptr);
	EXIT;
}
