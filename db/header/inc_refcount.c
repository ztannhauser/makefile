#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/struct.h"

#include "./struct.h"

void db_header_inc_refcount(struct db* db, size_t header_ptr)
{
	ENTER;
	int refcount;
	int fd = db->trees.fd;
	pread(fd, &refcount, sizeof(int),
		header_ptr + 
		sizeof(struct node_header) + 
		offsetof(struct header, refcount));
	verpv(refcount);
	refcount++;
	pwrite(fd, &refcount, sizeof(int),
		header_ptr + 
		sizeof(struct node_header) + 
		offsetof(struct header, refcount));
	EXIT;
}
