#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "./struct.h"

int db_compare_reprocess_src(struct fileavl* this, size_t a, size_t b)
{
	int ret;
	ENTER;
	verpv(this);
	verpv(a);
	verpv(b);
	int fd = this->fd;
	struct reprocess_src a_data;
	lseek(fd, a + sizeof(struct node_header), SEEK_SET);
	read(fd, &a_data, sizeof(struct reprocess_src));
	struct reprocess_src b_data;
	lseek(fd, b + sizeof(struct node_header), SEEK_SET);
	read(fd, &b_data, sizeof(struct reprocess_src));
	verpv(a_data.prior);
	verpv(b_data.prior);
	if(!(ret = a_data.prior - b_data.prior))
	{
		verpv(a_data.source_ptr);
		verpv(b_data.source_ptr);
		if(a_data.source_ptr == b_data.source_ptr)
		{
			ret = 0;
		}
		else
		{
			ret = a_data.source_ptr - b_data.source_ptr;
		}
		verpv(ret);
	}
	EXIT;
	return ret;
}
