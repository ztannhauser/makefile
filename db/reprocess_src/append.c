#include <stdlib.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/struct.h"
#include "db/tree_ids.h"

#include "./priority.h"
#include "./struct.h"
#include "./finder.h"

size_t db_reprocess_src_append(struct db* db, enum priority prior, size_t source_ptr)
{
	size_t ret;
	ENTER;
	verpv(prior);
	verpv(source_ptr);
	struct reprocess_src rs =
	{
		.prior = prior,
		.source_ptr = source_ptr
	};
	size_t loc = fileavl_find(&(db->trees),
		ti_reprocess_src,
		db_reprocess_src_finder,
		&rs);
	verpv(loc);
	if(loc)
	{
		ret = 0; // signal that record already exists
	}
	else
	{
		size_t ptr = fileavl_insert(db, ti_reprocess_src,
			&rs, sizeof(struct reprocess_src));
		verpv(ptr);
		ret = ptr;
	}
	EXIT;
	return ret;
}
