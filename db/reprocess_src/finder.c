#include <stdlib.h>
#include <unistd.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "db/super/compare.h"

#include "./struct.h"

int db_reprocess_src_finder(struct fileavl* this, size_t a, struct reprocess_src* b)
{
	int ret;
	ENTER;
	verpv(this);
	verpv(a);
	verpv(b);
	int fd = this->fd;
	struct reprocess_src a_data;
	lseek(fd,
		a +
		sizeof(struct node_header),
		SEEK_SET);
	read(fd, &a_data.prior, sizeof(a_data.prior));
	verpv(a_data.prior);
	verpv(b->prior);
	if(!(ret = a_data.prior - b->prior))
	{
		read(fd, &a_data.source_ptr, sizeof(size_t));
		verpv(a_data.source_ptr);
		verpv(b->source_ptr);
		if(a_data.source_ptr == b->source_ptr)
		{
			ret = 0;
		}
		else
		{
			ret = db_compare_super(this, a_data.source_ptr, b->source_ptr);
		}
		verpv(ret);
	}
	EXIT;
	return ret;
}
