#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "db/tree_ids.h"

#include "./struct.h"

void db_reprocess_src_foreach(struct db* db,
	void (*callback)(size_t src_ptr))
{
	ENTER;
	fileavl_for_each_node_cached(db, ti_reprocess_src, ({
		void mycallback(int fd, size_t node)
		{
			ENTER;
			verpv(node);
			size_t src_ptr;
			pread(fd, &src_ptr, sizeof(size_t),
				node + 
				sizeof(struct node_header) + 
				offsetof(struct reprocess_src, source_ptr));
			verpv(src_ptr);
			callback(src_ptr);
			fileavl_remove(db, ti_reprocess_src, node);
			EXIT;
		}
		mycallback;
	}));
	EXIT;
}
