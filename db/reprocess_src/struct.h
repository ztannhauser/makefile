
#include "priority.h"

struct reprocess_src 
{
	enum priority prior; // assumed to be first
	size_t source_ptr; // assumed to be second
} __attribute__((packed)); 
