#ifndef _home_zander_git_makefile2_db_tree_ids_h
#define _home_zander_git_makefile2_db_tree_ids_h
enum tree_id
{
	ti_holes,
	ti_arch_mk,
	ti_project_type,
	ti_reprocess_dir, // queue: (ptr to dir): might delete record, might rewrite)
	ti_reprocess_src, // queue: (ptr to source): calls CPP and rewrites
	ti_reprocess_hdr, // queue: (ptr to header): might delete header
	ti_sources, // (path, ptr to parent, timestamp, ptr to reprocess_src)
	ti_headers, // (path, timestamp)
	ti_dependencies_source, // (ptr to source, ptr to header)
	ti_dependencies_headers, // (ptr to above), sorted by header
	ti_directories, // (path, ptr to parent, refcount)
	ti_directories_sources, // (ptr to directory, ptr to source file)
	ti_directories_headers, // (ptr to directory, ptr to header file)
	ti_directories_subdirs, // (ptr to directory, ptr to subdirectory)
	ti_n
};
#endif

