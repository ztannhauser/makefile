#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "db/struct.h"
#include "db/tree_ids.h"

#include "./struct.h"

size_t db_dependency_create(struct db* db, size_t source_ptr, size_t header_ptr)
{
	ENTER;
	struct dependency new = 
	{
		.source_ptr = source_ptr,
		.header_ptr = header_ptr,
		.ptr_to_header_rec = 0,
	};
	size_t ptr = fileavl_insert(db,
		ti_dependencies_source, &new, sizeof(struct dependency));
	verpv(ptr);
	size_t ptr2 = fileavl_insert(db,
		ti_dependencies_headers, &ptr, sizeof(size_t));
	verpv(ptr2);
	pwrite(db->trees.fd, &ptr2, sizeof(size_t),
		ptr + 
		sizeof(struct node_header) + 
		offsetof(struct dependency, ptr_to_header_rec));
	EXIT;
	return ptr;
}
