#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "db/struct.h"
#include "db/tree_ids.h"

#include "./struct.h"

void db_dependency_destory(struct db* db, size_t dep_rec)
{
	ENTER;
	size_t reverse_ptr;
	pread(db->trees.fd, &reverse_ptr, sizeof(size_t),
		dep_rec + 
		sizeof(struct node_header) + 
		offsetof(struct dependency, ptr_to_header_rec));
	verpv(dep_rec);
	verpv(reverse_ptr);
	fileavl_remove(db, ti_dependencies_source, dep_rec);
	fileavl_remove(db, ti_dependencies_headers, reverse_ptr);
	EXIT;
}
