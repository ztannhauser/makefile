
void db_dependency_foreach_header_of_source(struct db* db,
	size_t source_ptr, void (*callback)(size_t dep_ptr, size_t hdr_ptr));
