#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "db/struct.h"
#include "db/tree_ids.h"

#include "./struct.h"

void db_dependency_foreach_header_of_source(struct db* db,
	size_t source_ptr, void (*callback)(size_t dep_ptr, size_t hdr_ptr))
{
	ENTER;
	verpv(db);
	verpv(source_ptr);
	int fd = db->trees.fd;
	fileavl_for_each_filtered(db, ti_dependencies_source,
	({
		int compare(struct fileavl* this, size_t loc, void* unused)
		{
			int ret;
			ENTER;
			verpv(loc);
			size_t a_source_ptr;
			pread(fd, &a_source_ptr, sizeof(size_t),
				loc + 
				sizeof(struct node_header) + 
				offsetof(struct dependency, source_ptr));
			verpv(a_source_ptr);
			ret = a_source_ptr - source_ptr;
			EXIT;
			return ret;
		}
		compare;
	}), NULL,
	({
		void mycallback(size_t loc)
		{
			ENTER;
			size_t hdr_ptr;
			pread(fd, &hdr_ptr, sizeof(size_t),
				loc + 
				sizeof(struct node_header) + 
				offsetof(struct dependency, header_ptr));
			verpv(hdr_ptr);
			callback(loc, hdr_ptr);
			EXIT;
		}
		mycallback;
	}));
	EXIT;
}


















