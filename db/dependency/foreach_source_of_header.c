#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "db/struct.h"
#include "db/tree_ids.h"

#include "./struct.h"

void db_dependency_foreach_source_of_header(struct db* db,
	size_t b_header_ptr, void (*callback)(size_t source_ptr))
{
	ENTER;
	int fd = db->trees.fd;
	fileavl_for_each_filtered(db, ti_dependencies_headers,
	({
		int compare(struct fileavl* this, size_t loc, void* unused)
		{
			int ret;
			ENTER;
			size_t ptr;
			pread(fd, &ptr, sizeof(size_t),
				loc + 
				sizeof(struct node_header));
			verpv(ptr);
			size_t a_header_ptr;
			pread(fd, &a_header_ptr, sizeof(size_t),
				ptr + 
				sizeof(struct node_header) + 
				offsetof(struct dependency, header_ptr));
			verpv(a_header_ptr);
			ret = a_header_ptr - b_header_ptr;
			verpv(ret);
			EXIT;
			return ret;
		}
		compare;
	}), NULL,
	({
		void mycallback(size_t loc)
		{
			ENTER;
			size_t ptr;
			pread(fd, &ptr, sizeof(size_t),
				loc + 
				sizeof(struct node_header));
			verpv(ptr);
			size_t source_ptr;
			pread(fd, &source_ptr, sizeof(size_t),
				ptr + 
				sizeof(struct node_header) + 
				offsetof(struct dependency, source_ptr));
			verpv(source_ptr);
			callback(source_ptr);
			EXIT;
		}
		mycallback;
	}));
	EXIT;
}







