
void db_dependency_foreach_source_of_header(struct db* db,
	size_t header_ptr, void (*callback)(size_t source_ptr));
