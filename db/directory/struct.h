#ifndef _home_zander_git_makefile2_db_directory_struct_h
#define _home_zander_git_makefile2_db_directory_struct_h

#include <linux/limits.h>

#include "db/super/struct.h"

struct directory
{
	struct super super;
	size_t parent_ptr;
	int refcount;
} __attribute__((packed));

#endif

