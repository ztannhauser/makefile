#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "db/struct.h"
#include "db/tree_ids.h"
#include "db/super/create.h"

#include "struct.h"

size_t db_directory_create(
	struct db* db, char* path, size_t parent_ptr)
{
	ENTER;
	verpvs(path);
	struct directory dir;
	db_super_create(&dir, path);
	dir.refcount = 0;
	dir.parent_ptr = parent_ptr;
	size_t ptr = fileavl_insert(&(db->trees),
		ti_directories,
		&dir, sizeof(struct directory));
	verpv(ptr);
	EXIT;
	return ptr;
}
