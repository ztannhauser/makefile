#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/struct.h"

#include "./struct.h"

int db_directory_dec_refcount(struct db* db, size_t directory_ptr)
{
	ENTER;
	size_t refcount;
	verpv(db);
	verpv(directory_ptr);
	int fd = db->trees.fd;
	off_t loc = 
		directory_ptr + 
		sizeof(struct node_header) + 
		offsetof(struct directory, refcount);
	pread(fd, &refcount, sizeof(size_t), loc);
	verpv(refcount);
	refcount--;
	pwrite(fd, &refcount, sizeof(size_t), loc);
	EXIT;
}
