#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/struct.h"

#include "./struct.h"

int db_directory_get_refcount(struct db* db, size_t directory_ptr)
{
	ENTER;
	verpv(directory_ptr);
	int refcount;
	pread(db->trees.fd, &refcount, sizeof(int),
		directory_ptr +
		sizeof(struct node_header) + 
		offsetof(struct directory, refcount));
	verpv(refcount);
	EXIT;
	return refcount;
}
