#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/struct.h"
#include "db/tree_ids.h"

#include "struct.h"

void db_directory_header_add(struct db* db, size_t directory_ptr, size_t header_ptr)
{
	ENTER;
	verpv(directory_ptr);
	verpv(header_ptr);
	struct directory_header df = 
	{
		.directory_ptr = directory_ptr,
		.header_ptr = header_ptr
	};
	size_t ptr = fileavl_insert(&(db->trees),
		ti_directories_headers,
		&df, sizeof(struct directory_header));
	verpv(ptr);
	EXIT;
}
