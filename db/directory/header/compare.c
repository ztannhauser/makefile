#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "struct.h"

int db_compare_directory_headers(struct fileavl* this, size_t a, size_t b)
{
	int ret;
	ENTER;
	verpv(a);
	verpv(b);
	int fd = this->fd;
	verpv(fd);
	size_t a_data, b_data;
	pread(fd, &a_data, sizeof(size_t),
		a + 
		sizeof(struct node_header) + 
		offsetof(struct directory_header, directory_ptr));
	pread(fd, &b_data, sizeof(size_t),
		b + 
		sizeof(struct node_header) + 
		offsetof(struct directory_header, directory_ptr));
	verpv(a_data);
	verpv(b_data);
	if(!(ret = a_data - b_data))
	{
		pread(fd, &a_data, sizeof(size_t),
			a + 
			sizeof(struct node_header) + 
			offsetof(struct directory_header, header_ptr));
		pread(fd, &b_data, sizeof(size_t),
			b + 
			sizeof(struct node_header) + 
			offsetof(struct directory_header, header_ptr));
		verpv(a_data);
		verpv(b_data);
		ret = a_data - b_data;
		verpv(ret);
	}
	EXIT;
	return ret;
}














