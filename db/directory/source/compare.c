#include <stdlib.h>
#include <unistd.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "struct.h"

int db_compare_directory_sources(struct fileavl* this, size_t a, size_t b)
{
	ENTER;
	verpv(a);
	verpv(b);
	struct directory_source a_data, b_data;
	int fd = this->fd;
	verpv(fd);
	lseek(fd, a + sizeof(struct node_header), SEEK_SET);
	read(fd, &a_data, sizeof(struct directory_source));
	lseek(fd, b + sizeof(struct node_header), SEEK_SET);
	read(fd, &b_data, sizeof(struct directory_source));
	verpv(a_data.directory_ptr);
	verpv(b_data.directory_ptr);
	verpv(a_data.source_ptr);
	verpv(b_data.source_ptr);
	int ret =
		a_data.directory_ptr - b_data.directory_ptr
		?:
		a_data.source_ptr - b_data.source_ptr;
	verpv(ret);
	EXIT;
	return ret; 
}
