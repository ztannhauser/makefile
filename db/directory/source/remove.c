#include <stdlib.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "db/struct.h"
#include "db/tree_ids.h"

void db_directory_source_remove(struct db* db, size_t dir_src_ptr)
{
	ENTER;
	fileavl_remove(&(db->trees),
		ti_directories_sources, dir_src_ptr);
	EXIT;
}
