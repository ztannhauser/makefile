
void db_directory_source_foreach(struct db* db,
	size_t b_directory_ptr, void (*callback)(size_t rec_ptr, size_t source_ptr));
