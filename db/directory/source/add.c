#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/struct.h"
#include "db/tree_ids.h"

#include "struct.h"

void db_directory_source_add(struct db* db, size_t directory_ptr, size_t source_ptr)
{
	ENTER;
	verpv(directory_ptr);
	verpv(source_ptr);
	struct directory_source df = 
	{
		.directory_ptr = directory_ptr,
		.source_ptr = source_ptr
	};
	size_t ptr = fileavl_insert(&(db->trees),
		ti_directories_sources,
		&df, sizeof(struct directory_source));
	verpv(ptr);
	EXIT;
}
