#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/struct.h"

#include "./struct.h"

size_t db_directory_get_parent(struct db* db, size_t directory_ptr)
{
	size_t parent;
	ENTER;
	verpv(directory_ptr);
	pread(db->trees.fd, &parent, sizeof(size_t),
		directory_ptr + 
		sizeof(struct node_header) + 
		offsetof(struct directory, parent_ptr));
	verpv(parent);
	EXIT;
	return parent;
}
