#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "db/struct.h"

#include "./struct.h"

int db_subdir_finder(struct fileavl* this, size_t a, struct directory_subdir* b_data)
{
	int ret;
	ENTER;
	verpv(a);
	int fd = this->fd;
	struct directory_subdir a_data;
	off_t a_loc = a + sizeof(struct node_header);
	pread(fd, &a_data, sizeof(a_data.directory_ptr), a_loc);
	verpv(a_data.directory_ptr);
	verpv(b_data->directory_ptr);
	if(!(ret = a_data.directory_ptr - b_data->directory_ptr))
	{
		pread(fd, &a_data.subdir_ptr, sizeof(a_data.subdir_ptr),
			a_loc + offsetof(struct directory_subdir, subdir_ptr));
		verpv(a_data.subdir_ptr);
		verpv(b_data->subdir_ptr);
		ret = a_data.subdir_ptr - b_data->subdir_ptr;
	}
	EXIT;
	return ret;
}
