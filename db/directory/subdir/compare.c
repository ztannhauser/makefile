#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "./struct.h"

int db_compare_directory_subdirs(struct fileavl* this, size_t a, size_t b)
{
	int ret;
	ENTER;
	verpv(a);
	verpv(b);
	int fd = this->fd;
	struct directory_subdir a_data, b_data;
	off_t a_loc = a + sizeof(struct node_header);
	off_t b_loc = b + sizeof(struct node_header);
	pread(fd, &a_data, sizeof(a_data.directory_ptr), a_loc);
	pread(fd, &b_data, sizeof(b_data.directory_ptr), b_loc);
	verpv(a_data.directory_ptr);
	verpv(b_data.directory_ptr);
	if(!(ret = a_data.directory_ptr - b_data.directory_ptr))
	{
		pread(fd, &a_data.subdir_ptr, sizeof(a_data.subdir_ptr),
			a_loc + offsetof(struct directory_subdir, subdir_ptr));
		pread(fd, &b_data.subdir_ptr, sizeof(b_data.subdir_ptr),
			b_loc + offsetof(struct directory_subdir, subdir_ptr));
		verpv(a_data.subdir_ptr);
		verpv(b_data.subdir_ptr);
		ret = a_data.subdir_ptr - b_data.subdir_ptr;
	}
	EXIT;
	return ret;
}
