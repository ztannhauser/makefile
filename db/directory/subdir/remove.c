#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>
#include <assert.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/struct.h"
#include "db/tree_ids.h"

#include "./struct.h"
#include "./finder.h"

void db_directory_subdir_remove(struct db* db,
	size_t directory_ptr, size_t subdir_ptr)
{
	ENTER;
	verpv(directory_ptr);
	verpv(subdir_ptr);
	struct directory_subdir df = 
	{
		.directory_ptr = directory_ptr,
		.subdir_ptr = subdir_ptr
	};
	size_t ptr = fileavl_find(&(db->trees),
		ti_directories_subdirs,
		db_subdir_finder, &df);
	verpv(ptr);
	assert(ptr);
	fileavl_remove(db, ti_directories_subdirs, ptr);
	EXIT;
}
