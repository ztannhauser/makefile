#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "db/struct.h"
#include "db/tree_ids.h"

#include "./struct.h"

void db_directory_subdir_foreach(struct db* db,
	size_t b_directory_ptr, void (*callback)(size_t))
{
	ENTER;
	verpv(b_directory_ptr);
	int compare(struct fileavl* avl, size_t loc, void* b)
	{
		int ret;
		ENTER;
		verpv(loc);
		size_t a_directory_ptr;
		pread(avl->fd, &a_directory_ptr, sizeof(size_t),
			loc + sizeof(struct node_header));
		verpv(a_directory_ptr);
		verpv(b_directory_ptr);
		ret = a_directory_ptr - b_directory_ptr;
		verpv(ret);
		EXIT;
		return ret;
	}
	int fd = db->trees.fd;
	fileavl_for_each_filtered(db, ti_directories_subdirs, compare, NULL, ({
		void mycallback(size_t directory_subdir_ptr)
		{
			ENTER;
			verpv(directory_subdir_ptr);
			size_t subdir_ptr;
			pread(fd, &subdir_ptr, sizeof(size_t),
				directory_subdir_ptr + 
				sizeof(struct node_header) + 
				offsetof(struct directory_subdir, subdir_ptr));
			verpv(subdir_ptr);
			callback(subdir_ptr);
			EXIT;
		}
		mycallback;
	}));
	EXIT;
}
