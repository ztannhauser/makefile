#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/struct.h"
#include "db/tree_ids.h"

#include "struct.h"

void db_directory_subdir_add(struct db* db, size_t directory_ptr, size_t subdir_ptr)
{
	ENTER;
	verpv(directory_ptr);
	verpv(subdir_ptr);
	struct directory_subdir df = 
	{
		.directory_ptr = directory_ptr,
		.subdir_ptr = subdir_ptr
	};
	size_t ptr = fileavl_insert(&(db->trees),
		ti_directories_subdirs,
		&df, sizeof(struct directory_subdir));
	verpv(ptr);
	EXIT;
}
