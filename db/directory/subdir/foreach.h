
void db_directory_subdir_foreach(struct db* db,
	size_t directory_ptr, void (*callback)(size_t));
