#include <stdlib.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "db/tree_ids.h"
#include "db/struct.h"

void db_directory_destory(struct db* db, size_t dir_ptr)
{
	ENTER;
	fileavl_remove(&(db->trees),
		ti_directories, dir_ptr);
	EXIT;
}
