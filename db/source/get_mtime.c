#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/struct.h"

#include "./struct.h"

time_t db_source_get_mtime(struct db* db, size_t source_ptr)
{
	ENTER;
	verpv(source_ptr);
	time_t mtime;
	pread(db->trees.fd, &mtime, sizeof(time_t),
		source_ptr + 
		sizeof(struct node_header) + 
		offsetof(struct source, mtime));
	verpv(mtime);
	EXIT;
	return mtime;
}
