#include <stdlib.h>
#include <stddef.h>
#include <unistd.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/struct.h"

#include "./struct.h"

size_t db_source_get_queue_ptr(struct db* db, size_t source_ptr)
{
	size_t queue_ptr;
	ENTER;
	pread(db->trees.fd, &queue_ptr, sizeof(size_t),
		source_ptr + 
		sizeof(struct node_header) + 
		offsetof(struct source, queue_ptr));
	verpv(queue_ptr);
	EXIT;
	return queue_ptr;
}
