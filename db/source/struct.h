#include <linux/limits.h>

#include "db/super/struct.h"

struct source
{
	struct super super;
	size_t parent_ptr;
	int strlen_filename;
	time_t mtime;
	size_t queue_ptr;
} __attribute__((packed));
