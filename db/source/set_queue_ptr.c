#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/struct.h"

#include "./struct.h"

void db_source_set_queue_ptr(struct db* db, size_t source_ptr, size_t queue_ptr)
{
	ENTER;
	verpv(source_ptr);
	verpv(queue_ptr);
	int fd = db->trees.fd;
	off_t loc =
		source_ptr +
		sizeof(struct node_header) + 
		offsetof(struct source, queue_ptr);
	lseek(fd, loc, SEEK_SET);
	write(fd, &queue_ptr, sizeof(size_t));
	EXIT;
}
