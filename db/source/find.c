#include <stdlib.h>
#include <stdio.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "db/struct.h"
#include "db/tree_ids.h"
#include "db/super/finder.h"

size_t db_source_find(struct db* db, const char* path)
{
	ENTER;
	size_t ptr = fileavl_find(&db->trees,
		ti_sources,
		db_super_finder,
		path);
	verprintf("ptr == %lu\n", ptr);
	EXIT;
	return ptr;
}
