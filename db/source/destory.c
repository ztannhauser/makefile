#include <stdlib.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "db/tree_ids.h"

void db_source_destory(struct db* db, size_t source_ptr)
{
	ENTER;
	fileavl_remove(db,
		ti_sources, source_ptr);
	EXIT;
}
