#include <stdlib.h>
#include <string.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"
#include "db/struct.h"
#include "db/tree_ids.h"
#include "db/super/create.h"

#include "./struct.h"
#include "./set_mtime.h"
#include "./set_queue_ptr.h"

size_t db_source_create(struct db* db, char* path,
	size_t parent_ptr, int strlen_filename, time_t mtime)
{
	ENTER;
	verpvs(path);
	verpv(mtime);
	verpv(strlen_filename);
	struct source src;
	verpv(sizeof(struct source));
	db_super_create(&src, path);
	src.mtime = mtime;
	src.parent_ptr = parent_ptr;
	src.strlen_filename = strlen_filename;
	src.queue_ptr = 0;
	verpv(src.mtime);
	size_t ptr = fileavl_insert(db,
		ti_sources, &src, sizeof(struct source));
	verpv(ptr);
	EXIT;
	return ptr;
}
