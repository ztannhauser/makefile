#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/struct.h"

#include "./struct.h"

int db_source_get_filename(struct db* db, size_t source_ptr, char* out_filename)
{
	ENTER;
	verpv(source_ptr);
	int fd = db->trees.fd;
	int strlen_path;
	pread(fd, &strlen_path, sizeof(int),
		source_ptr +
		sizeof(struct node_header));
	verpv(strlen_path);
	int strlen_filename;
	pread(fd, &strlen_filename, sizeof(int),
		source_ptr +
		sizeof(struct node_header) + 
		offsetof(struct source, strlen_filename));
	verpv(strlen_filename);
	pread(fd, out_filename, strlen_filename + 1,
		source_ptr +
		sizeof(struct node_header) + 
		offsetof(struct super, path) + 
		strlen_path - strlen_filename);
	verpvs(out_filename);
	EXIT;
	return strlen_filename;
}
