#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/struct.h"

#include "./struct.h"

size_t db_source_get_parent(struct db* db, size_t source_ptr)
{
	size_t parent;
	ENTER;
	verpv(source_ptr);
	pread(db->trees.fd, &parent, sizeof(size_t),
		source_ptr + 
		sizeof(struct node_header) + 
		offsetof(struct source, parent_ptr));
	verpv(parent);
	EXIT;
	return parent;
}
