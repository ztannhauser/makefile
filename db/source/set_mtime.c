#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"

#include "libfileavl.h"

#include "db/struct.h"

#include "./struct.h"

void db_source_set_mtime(struct db* db, size_t source_ptr, time_t time)
{
	ENTER;
	verpv(source_ptr);
	verpv(time);
	pwrite(db->trees.fd, &time, sizeof(time_t),
		source_ptr + 
		sizeof(struct node_header) + 
		offsetof(struct source, mtime));
	EXIT;
}
