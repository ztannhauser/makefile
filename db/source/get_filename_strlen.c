#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/struct.h"

#include "./struct.h"

int db_source_get_filename_strlen(struct db* db, size_t source_ptr)
{
	int ret;
	ENTER;
	pread(db->trees.fd, &ret, sizeof(int),
		source_ptr + 
		sizeof(struct node_header) + 
		offsetof(struct source, strlen_filename));
	verpv(ret);
	EXIT;
	return ret;
}
