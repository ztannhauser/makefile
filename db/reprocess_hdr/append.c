#include <stdlib.h>
#include <linux/limits.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/struct.h"
#include "db/tree_ids.h"
#include "db/super/get_path.h"

#include "./finder.h"

void db_reprocess_hdr_append(struct db* db, size_t header_ptr)
{
	ENTER;
	verpv(header_ptr);
	size_t loc = fileavl_find(&(db->trees),
		ti_reprocess_hdr,
		db_reprocess_hdr_finder,
		&header_ptr);
	verpv(loc);
	if(!loc)
	{
		size_t ptr = fileavl_insert(db, ti_reprocess_hdr,
			&header_ptr, sizeof(size_t));
		verpv(ptr);
	}
	EXIT;
}
