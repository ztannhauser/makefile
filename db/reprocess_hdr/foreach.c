#include <stdlib.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "db/tree_ids.h"

void db_reprocess_hdr_foreach(struct db* db,
	void (*callback)(size_t hdr_ptr))
{
	ENTER;
	fileavl_for_each_node_cached(db, ti_reprocess_hdr, ({
		void mycallback(int fd, size_t node)
		{
			ENTER;
			verpv(node);
			size_t hdr_ptr;
			pread(fd, &hdr_ptr, sizeof(size_t),
				node + 
				sizeof(struct node_header));
			verpv(hdr_ptr);
			callback(hdr_ptr);
			fileavl_remove(db, ti_reprocess_hdr, node);
			EXIT;
		}
		mycallback;
	}));
	EXIT;
}
