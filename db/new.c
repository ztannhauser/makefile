#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "struct.h"
#include "defines.h"
#include "./comparers.h"
#include "./directory/create.h"
#include "./project_type/type.h"
#include "./project_type/set.h"

struct db* new_db(bool* first_time)
{
	ENTER;
	struct db* this = malloc(sizeof(struct db));
	verpv(this);
	int fileavl_open_ret = fileavl_open(&(this->trees),
		DB_FILENAME, O_CREAT | O_RDWR, 0666,
		db_compares, ti_n, true);
	if(fileavl_open_ret < 0)
	{
		perror("fileavl_open");
	}
	else
	{
		if(*first_time = (fileavl_open_ret == 1))
		{
			db_directory_create(this, ".", 0);
		}
	}
	EXIT;
	return this;
}
