#include <unistd.h>
#include <stdlib.h>

#define DEBUG 0
#include "libdebug.h"

#include "struct.h"

void delete_db(struct db* this)
{
	ENTER;
	fileavl_close(&this->trees);
	free(this);
	EXIT;
}
