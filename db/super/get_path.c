#include <stdlib.h>
#include <unistd.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

int db_super_get_path(struct fileavl* this, size_t super_ptr, char* out_path)
{
	ENTER;
	lseek(this->fd,
		super_ptr + 
		sizeof(struct node_header),
		SEEK_SET);
	int len;
	read(this->fd, &len, sizeof(int));
	verpv(len);
	read(this->fd, out_path, len + 1);
	verpvs(out_path);
	EXIT;
	return len;
}
