#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/limits.h>
#include <string.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "db/super/get_path.h"

int db_compare_super(struct fileavl* this, size_t a, size_t b)
{
	int ret;
	ENTER;
	int fd = this->fd;
	int strlen_a_path, strlen_b_path;
	char a_path[PATH_MAX], b_path[PATH_MAX];
	strlen_a_path = db_super_get_path(this, a, a_path);
	strlen_b_path = db_super_get_path(this, b, b_path);
	verpvs(a_path);
	verpvs(b_path);
	ret = strcmp(a_path, b_path);
	verpv(ret);
	EXIT;
	return ret;
};
