#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <linux/limits.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"
#include "libdebug.h"

#include "db/super/get_path.h"

int db_super_finder(struct fileavl* this, size_t a, void* b_path)
{
	ENTER;
	verpv(a);
	char path[PATH_MAX];
	int strlen_path = db_super_get_path(this, a, path);
	verpvs(path);
	verpvs(b_path);
	int ret = strcmp(path, b_path);
	verpv(ret);
	EXIT;
	return ret;
}
