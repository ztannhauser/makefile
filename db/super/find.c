#include <stdlib.h>
#include <stdio.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "db/struct.h"
#include "db/tree_ids.h"

#include "./finder.h"

size_t db_super_find(struct db* db, enum tree_id id, const char* path)
{
	ENTER;
	size_t ptr = fileavl_find(&db->trees,
		id, db_super_finder,
		path);
	verprintf("ptr == %lu\n", ptr);
	EXIT;
	return ptr;
}
