#include <stdlib.h>
#include <string.h>
#include <linux/limits.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "db/struct.h"
#include "db/tree_ids.h"

#include "./struct.h"

void db_super_create(
	struct super* this,
	char* path
)
{
	ENTER;
	verpvs(path);
	int len = this->strlen_path = strlen(path);
	verpv(len);
	memset(this->path, 0, PATH_MAX);
	memcpy(this->path, path, len + 1);
	EXIT;
}
