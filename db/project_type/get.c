#include "db/struct.h"
#include "db/tree_ids.h"

#include "type.h"

enum project_type db_project_type_get(struct db* this)
{
	return this->trees.roots[ti_project_type];
}
