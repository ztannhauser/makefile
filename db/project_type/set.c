#include "db/struct.h"
#include "db/tree_ids.h"

#include "type.h"

void db_project_type_set(struct db* this, enum project_type new)
{
	this->trees.roots[ti_project_type] = new;
	this->trees.should_flush = true;
}
