
#include <linux/limits.h>
#include <unistd.h>
#include <stddef.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "db/super/finder.h"

int db_reprocess_dir_finder(struct fileavl* this, size_t a, size_t* b_directory_ptr)
{
	int ret;
	ENTER;
	verpv(a);
	int fd = this->fd;
	verpv(fd);
	size_t a_directory_ptr;
	pread(fd, &a_directory_ptr, sizeof(size_t),
		a + 
		sizeof(struct node_header));
	ret = a_directory_ptr - *b_directory_ptr;
	EXIT;
	return ret;
}
