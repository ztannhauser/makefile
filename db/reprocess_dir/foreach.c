#include <stdlib.h>
#include <unistd.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "db/tree_ids.h"

void db_reprocess_dir_foreach(struct db* db,
	void (*callback)(size_t dir_ptr))
{
	ENTER;
	fileavl_for_each_node_cached(db, ti_reprocess_dir, ({
		void mycallback(int fd, size_t node)
		{
			ENTER;
			verpv(node);
			size_t dir_ptr;
			pread(fd, &dir_ptr, sizeof(size_t),
				node + 
				sizeof(struct node_header));
			verpv(dir_ptr);
			callback(dir_ptr);
			fileavl_remove(db, ti_reprocess_dir, node);
			EXIT;
		}
		mycallback;
	}));
	EXIT;
}
