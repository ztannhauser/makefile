#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

int db_compare_reprocess_dir(struct fileavl* this, size_t a, size_t b)
{
	int ret;
	ENTER;
	int fd = this->fd;
	size_t a_data, b_data;
	pread(fd, &a_data, sizeof(size_t), a + sizeof(struct node_header));
	pread(fd, &b_data, sizeof(size_t), b + sizeof(struct node_header));
	verpv(a_data);
	verpv(b_data);
	ret = a_data - b_data;
	verpv(ret);
	EXIT;
	return ret;
};
