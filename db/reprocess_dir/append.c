#include <stdlib.h>
#include <linux/limits.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/struct.h"
#include "db/tree_ids.h"

#include "./finder.h"

void db_reprocess_dir_append(struct db* db, size_t directory_ptr)
{
	ENTER;
	size_t loc = fileavl_find(&(db->trees),
		ti_reprocess_dir,
		db_reprocess_dir_finder,
		&directory_ptr);
	verpv(loc);
	if(!loc)
	{
		size_t ptr = fileavl_insert(db, ti_reprocess_dir,
			&directory_ptr, sizeof(size_t));
		verpv(ptr);
	}
	EXIT;
}
