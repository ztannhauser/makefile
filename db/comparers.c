#include <stdlib.h>

#include "libfileavl.h"

#include "db/tree_ids.h"

#include "reprocess_src/compare.h"
#include "reprocess_dir/compare.h"
#include "reprocess_hdr/compare.h"
#include "super/compare.h"
#include "dependency/compare_source.h"
#include "dependency/compare_header.h"
#include "directory/source/compare.h"
#include "directory/header/compare.h"
#include "directory/subdir/compare.h"

fileavl_comparator db_compares[ti_n] = 
{
	[ti_reprocess_src] = db_compare_reprocess_src,
	[ti_reprocess_dir] = db_compare_reprocess_dir,
	[ti_reprocess_hdr] = db_compare_reprocess_hdr,
	[ti_sources] = db_compare_super,
	[ti_headers] = db_compare_super,
	[ti_dependencies_source] = db_compare_dependency_source,
	[ti_dependencies_headers] = db_compare_dependency_header,
	[ti_directories] = db_compare_super,
	[ti_directories_sources] = db_compare_directory_sources,
	[ti_directories_headers] = db_compare_directory_headers,
	[ti_directories_subdirs] = db_compare_directory_subdirs,
	NULL
};
