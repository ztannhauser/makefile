#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdio.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"
#include "libarray.h"

#include "db/struct.h"
#include "db/tree_ids.h"
#include "struct.h"
#include "create_default.h"

void db_arch_mk_set_flags(struct db* db, struct array* flags)
{
	ENTER;
	size_t arch_mk_ptr = db->trees.roots[ti_arch_mk];
	if(arch_mk_ptr)
	{
		arch_mk_ptr += sizeof(struct node_header);
		struct arch_mk data;
		int fd = db->trees.fd;
		lseek(fd, arch_mk_ptr, SEEK_SET);
		read(fd, &data, sizeof(struct arch_mk));
		verprintf("before: data.n == %lu\n", data.n);
		if(data.n)
		{
			lseek(fd, data.data, SEEK_SET);
			for(size_t m = 0;m < data.n;m++)
			{
				size_t ptr;
				read(fd, &ptr, sizeof(size_t));
				fileavl_free(db, ptr);
			}
		}
		data.data = fileavl_realloc(db, data.data, sizeof(size_t) * flags->n);
		data.n = flags->n;
		verprintf("after: data.n == %lu\n", data.n);
		if(data.n)
		{
			for(size_t loc = data.data, m = 0;m < data.n;
				m++,loc += sizeof(size_t))
			{
				char* flag = array_index(*flags, m, char*);
				verprintf("flag == %s\n", flag);
				int len = strlen(flag) + 1;
				size_t ptr = fileavl_malloc(db, len);
				lseek(fd, ptr + sizeof(struct node_header), SEEK_SET);
				write(fd, flag, len);
				lseek(fd, loc, SEEK_SET);
				write(fd, &ptr, sizeof(size_t));
			}
		}
		lseek(fd, arch_mk_ptr, SEEK_SET);
		write(fd, &data, sizeof(struct arch_mk));
	}
	else
	{
		db_arch_mk_create_default(db);
	}
	EXIT;
}
