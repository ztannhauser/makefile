#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <stddef.h>
#include <string.h>

#define DEBUG 0
#include "libdebug.h"
#include "libarray.h"

#include "db/struct.h"
#include "db/tree_ids.h"

#include "struct.h"

size_t db_arch_mk_get_flags(struct db* db, struct array* args)
{
	ENTER;
	int fd = db->trees.fd;
	size_t arch_mk_ptr = db->trees.roots[ti_arch_mk];
	assert(arch_mk_ptr);
	size_t data;
	pread(fd, &data, sizeof(size_t),
		arch_mk_ptr +
		sizeof(struct node_header) + 
		offsetof(struct arch_mk, data));
	verpv(data);
	size_t n;
	pread(fd, &n, sizeof(size_t),
		arch_mk_ptr + 
		sizeof(struct node_header) + 
		offsetof(struct arch_mk, n));
	verpv(n);
	char c, nullchar = '\0';
	struct array flag = new_array(char);
	for(size_t i = 0, loc = data;i < n;i++, loc += sizeof(size_t))
	{
		size_t flag_ptr;
		pread(fd, &flag_ptr, sizeof(size_t), loc);
		verpv(flag_ptr);
		lseek(fd, flag_ptr + sizeof(struct node_header), SEEK_SET);
		while(read(fd, &c, 1) && c)
		{
			array_push_n(&flag, &c);
		}
		array_push_n(&flag, &nullchar);
		verpvs(flag.data);
		char* clone = memcpy(malloc(flag.n), flag.data, flag.n);
		array_push_n(args, &clone);
		array_clear(&flag);
	}
	array_delete(&flag);
	EXIT;
	return n;
}
