#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/struct.h"
#include "db/tree_ids.h"
#include "create_default.h"

void db_arch_mk_set_time(struct db* db, time_t time)
{
	ENTER;
	verprintf("db == %p\n", db);
	verprintf("db_arch_mk_set_time\n");
	size_t arch_mk_ptr = db->trees.roots[ti_arch_mk];
	verprintf("arch_mk_ptr == %lu\n", arch_mk_ptr);
	if(arch_mk_ptr)
	{
		verprintf("here\n");
		arch_mk_ptr += sizeof(struct node_header);
		int fd = db->trees.fd;
		lseek(fd, arch_mk_ptr, SEEK_SET);
		write(fd, &time, sizeof(time_t));
	}
	else
	{
		db_arch_mk_create_default(db);
	}
	EXIT;
}
