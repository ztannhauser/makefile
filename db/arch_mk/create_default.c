#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/struct.h"
#include "db/tree_ids.h"

#include "struct.h"

void db_arch_mk_create_default(struct db* db)
{
	ENTER;
	size_t
		arch_mk_ptr =
		db->trees.roots[ti_arch_mk] =
		fileavl_malloc(&(db->trees), sizeof(struct arch_mk))
	;
	verpv(arch_mk_ptr);
	arch_mk_ptr += sizeof(struct node_header);
	struct arch_mk default_arch_mk = 
	{
		.mtime = 0,
		.data = 0,
		.n = 0
	};
	int fd = db->trees.fd;
	lseek(fd, arch_mk_ptr, SEEK_SET);
	write(fd, &default_arch_mk, sizeof(struct arch_mk));
	db->trees.should_flush = true;
	EXIT;
}
