#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>
#include <stdio.h>

#define DEBUG 0
#include "libdebug.h"
#include "libfileavl.h"

#include "db/struct.h"
#include "db/tree_ids.h"

#include "struct.h"
#include "create_default.h"

bool db_arch_mk_is_outdated(struct db* db, time_t filetime)
{
	ENTER;
	verprintf("db == %p\n", db);
	bool ret;
	size_t arck_mk_ptr = db->trees.roots[ti_arch_mk];
	verprintf("arck_mk_ptr == %lu\n", arck_mk_ptr);
	if(arck_mk_ptr)
	{
		arck_mk_ptr += sizeof(struct node_header);
		int fd = db->trees.fd;
		time_t recorded_time;
		lseek(fd, arck_mk_ptr, SEEK_SET);
		read(fd, &recorded_time, sizeof(time_t));
		verprintf("recorded_time == %lu\n", recorded_time);
		ret = filetime > recorded_time;
	}
	else
	{
		db_arch_mk_create_default(db);
		ret = true;
	}
	EXIT;
	return ret;
}
