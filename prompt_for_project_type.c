#include <stdlib.h>
#include <stdio.h>

#include "db/project_type/type.h"
#include "db/project_type/set.h"

void prompt_for_project_type(struct db* db)
{
	char c = 'w';
	do
	{
		printf("Is this project a application or library? (a/l): ");
		fflush(stdout);
		scanf("%c", &c);
		getchar();
	}
	while(!(c == 'a' || c == 'l'));
	enum project_type type = pt_unset;
	switch(c)
	{
		case 'a': type = pt_application; break;
		case 'l': type = pt_library; break;
		default: abort();
	}
	db_project_type_set(db, type);
}
