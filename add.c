#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <assert.h>

#define DEBUG 0
#include "libdebug.h"

#include "db/tree_ids.h"

#include "db/struct.h"
#include "db/super/find.h"
#include "db/source/create.h"
#include "db/source/get_mtime.h"
#include "db/source/get_queue_ptr.h"
#include "db/source/get_filename_strlen.h"
#include "db/source/set_queue_ptr.h"
#include "db/directory/create.h"
#include "db/directory/inc_refcount.h"
#include "db/directory/source/add.h"
#include "db/directory/subdir/add.h"
#include "db/reprocess_dir/append.h"
#include "db/reprocess_src/append.h"

#include "./get_mtime.h"

bool is_dir(const char* path)
{
	bool ret;
	struct stat sb;
	int stat_ret = stat(path, &sb);
	if(stat_ret < 0)
	{
		ret = false;
	}
	else
	{
		ret = (sb.st_mode & S_IFMT) == S_IFDIR;
	}
	return ret;
}

void add(struct db* db)
{
	ENTER;
	size_t get_parent_dir(char* path)
	{
		size_t ret;
		ENTER;
		verpvs(path);
		// find last '/', replace with '\0'
		char* slash = rindex(path, '/');
		assert(slash);
		*slash = '\0';
		verpvs(path);
		// try to find dir record,
		size_t dir_ptr = db_super_find(db, ti_directories, path);
		verpv(dir_ptr);
		if(dir_ptr)
		{
			// return found record
			ret = dir_ptr;
		}
		else
		{
			// call self, save result
			size_t parent = get_parent_dir(path);
			verpv(parent);
			// create dir record, with result as parentdir
			size_t me = db_directory_create(
				db, path, parent);
			verpv(me);
			// add me as subdir to parentdir
			db_directory_subdir_add(db, parent, me);
			db_directory_inc_refcount(db, parent);
			// add parentdir to rewrite
			db_reprocess_dir_append(db, parent);
			ret = me;
		}
		*slash = '/';
		EXIT;
		return ret;
	}
	void go(char* path, char* end)
	{
		ENTER;
		verpvs(path);
		DIR* dir = opendir(path);
		*end++ = '/';
		for(struct dirent* dirent; dirent = readdir(dir);)
		{
			if(dirent->d_name[0] != '.')
			{
				verpvs(dirent->d_name);
				char* local_end = end;
				char* str = dirent->d_name;
				int len = strlen(str);
				memcpy(local_end, str, len + 1);
				local_end += len;
				if(is_dir(path))
				{
					go(path, local_end);
				}
				else
				{
					if(str[len - 1] == 'c' && str[len - 2] == '.')
					{
						verprintf("path == \"%s\"\n", path);
						size_t source_ptr = db_super_find(db, ti_sources, path);
						verprintf("source_ptr == %lu\n", source_ptr);
						if(!source_ptr)
						{
							printf("Added source file \"%s\" to queue\n",
								path);
							time_t mtime = get_mtime(path);
							verpv(mtime);
							size_t parent = get_parent_dir(path);
							verpv(parent);
							// size_t new_source_ptr = db_source_create(path);
							size_t new_source_ptr =
								db_source_create(db, path, parent,
									strlen(dirent->d_name), mtime);
							verpv(new_source_ptr);
							// add new source into parent dir
							db_directory_source_add(db, parent,
								new_source_ptr);
							db_directory_inc_refcount(db, parent);
							// append parent dir to rewrite queue
							db_reprocess_dir_append(db, parent);
							// append new source to process queue
							size_t queue_ptr =
								db_reprocess_src_append(db,
									p_added, new_source_ptr);
							assert(queue_ptr); 
						}
					}
				}
			}
		}
		closedir(dir);
		EXIT;
	}
	char path[PATH_MAX] = ".";
	go(path, path + 1);
	EXIT;
}






















