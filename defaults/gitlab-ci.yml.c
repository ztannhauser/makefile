const char* gitlab_ci_yml_content = 
""
"build:build:\n"
"  script:\n"
"   - projectname=$(basename $(pwd))\n"
"   - echo $projectname\n"
"   - cd ..\n"
"   - git clone https://gitlab.com/ztannhauser/ghost.git\n"
"   - python ./ghost/ghost.py $projectname\n"
"";
