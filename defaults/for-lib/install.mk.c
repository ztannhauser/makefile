const char* for_lib_install_mk_content = 
""
"projectname = $(shell basename $(shell pwd))\n"
"\n"
"install: installdeps ../$(projectname).a  ../$(projectname).h\n"
"\n"
"../$(projectname).a: main.a\n"
"	cp main.a ../$(projectname).a\n"
"\n"
"../$(projectname).h: public.h\n"
"	cp public.h ../$(projectname).h\n"
"";
