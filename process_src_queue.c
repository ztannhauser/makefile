#include <stdlib.h>
#include <linux/limits.h>
#include <stdbool.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdio.h>

#define DEBUG 0
#include "libdebug.h"
#include "libexec.h"
#include "libarray.h"
#include "libfileavl.h"

#include "get_mtime.h"

#include "db/tree_ids.h"
#include "db/super/get_path.h"
#include "db/super/find.h"
#include "db/source/set_queue_ptr.h"
#include "db/source/get_parent.h"
#include "db/source/get_filename_strlen.h"
#include "db/header/create.h"
#include "db/header/inc_refcount.h"
#include "db/header/dec_refcount.h"
#include "db/reprocess_src/foreach.h"
#include "db/reprocess_hdr/append.h"
#include "db/directory/header/add.h"
#include "db/dependency/create.h"
#include "db/dependency/destory.h"
#include "db/dependency/foreach_header_of_source.h"
#include "db/arch_mk/get_flags.h"

void process_src_queue(struct db* db)
{
	ENTER;
	struct array buildingpath = new_array(char);
	struct array cpp_command = new_array(char*);
	char nullchar = '\0';
	char *cpp = "cpp", *mm = "-MM", *mt = "-MT",
		*empty_string = "", *null = NULL;
	array_push_n(&cpp_command, &cpp);
	array_push_n(&cpp_command, &mm);
	array_push_n(&cpp_command, &mt);
	array_push_n(&cpp_command, &empty_string);
	int arch_flags_index = cpp_command.n;
	int arch_flags_n = db_arch_mk_get_flags(db, &cpp_command);
	char parent_dir_path[PATH_MAX];
	char source_makefile_path[PATH_MAX], object_file_path[PATH_MAX];
	char src_path[PATH_MAX], *src_path_ptr = src_path;
	db_reprocess_src_foreach(db, ({
		void callback(size_t src_ptr)
		{
			ENTER;
			db_super_get_path(db, src_ptr, src_path);
			verpvs(src_path);
			printf("Processing source file \"%s\"\n", src_path);
			size_t parent_src_ptr = db_source_get_parent(db, src_ptr);
			// get parent dir's path
			db_super_get_path(db, parent_src_ptr, parent_dir_path);
			verpvs(parent_dir_path);
			// get source filename
			char* src_filename =
				src_path_ptr +
				strlen(src_path_ptr) - 
				db_source_get_filename_strlen(db, src_ptr);
			verpvs(src_filename);
			sprintf(object_file_path, "%s/.%s.o", parent_dir_path, src_filename);
			sprintf(source_makefile_path, "%s/.%s.mk",
				parent_dir_path, src_filename);
			verpvs(object_file_path);
			verpvs(source_makefile_path);
			int mfd = open(source_makefile_path,
				O_CREAT | O_TRUNC | O_WRONLY, 0666);
			verpv(mfd);
			dprintf(mfd, "%s: %s", object_file_path, src_path);
			struct entry
			{
				bool should_remove;
				size_t dependency_ptr;
				size_t header_ptr;
			};
			// build dependecy list of source file,
			struct array entries = new_array(struct entry);
			db_dependency_foreach_header_of_source(db, src_ptr, ({
				void callback(size_t dep_ptr, size_t hdr_ptr)
				{
					ENTER;
					verpv(dep_ptr);
					verpv(hdr_ptr);
					struct entry e = 
					{
						.should_remove = true,
						.dependency_ptr = dep_ptr,
						.header_ptr = hdr_ptr
					};
					array_push_n(&entries, &e);
					EXIT;
				}
				callback;
			}));
			verpv(entries.n);
			// call CPP, pipe output
			array_push_n(&cpp_command, &src_path_ptr);
			array_push_n(&cpp_command, &null);
			verpv(cpp_command.n);
			int pfd[2];
			pipe(pfd);
			HERE;
			pid_t child = exec_wp(cpp_command.data, NULL, pfd);
			HERE;
			close(pfd[1]);
			HERE;
			array_pop_n(&cpp_command);
			array_pop_n(&cpp_command);
			char c;
			bool is_whitespace()
			{
				return c == ' ';
			}
			HERE;
			// parse output
			while(read(pfd[0], &c, 1) && c != '\n')
			{
				if(c == ':') continue;
				if(is_whitespace()) continue;
				if(c == '\\')
				{
					assert(read(pfd[1], &c, 1));
					continue;
				}
				do
				{
					if(c == '\\')
					{
						assert(read(pfd[1], &c, 1));
					}
					array_push_n(&buildingpath, &c);
				}
				while(read(pfd[0], &c, 1) && !is_whitespace());
				array_push_n(&buildingpath, &nullchar);
				verpvs(buildingpath.data);
				if(buildingpath.n > 3 &&
					!strcmp(arrayp_index(buildingpath,
						buildingpath.n - 3), ".h"))
				{
					dprintf(mfd, " %s", buildingpath.data);
					// lookup header records,
					size_t hdr_ptr = db_super_find(db,
						ti_headers, buildingpath.data);
					verpv(hdr_ptr);
					// if already exists:
					if(hdr_ptr)
					{
						// search dep list
						struct entry* found = bsearch(&hdr_ptr, entries.data,
							entries.n, sizeof(struct entry), ({
								int compare(void* unused, struct entry* ele)
								{
									return hdr_ptr - ele->header_ptr;
								}
								compare;
							}));
						verpv(found);
						// if found:
						if(found)
						{
							// flag to keep
							found->should_remove = false;
							// make sure to handle duplicates correctly
							array_pop(&entries,
								((unsigned char*) found - entries.data) /
									sizeof(struct entry));
						}
						else
						{
							// create dep rec
							size_t new_dep_ptr = db_dependency_create(db,
								src_ptr, hdr_ptr);
							verpv(new_dep_ptr);
							db_header_inc_refcount(db, hdr_ptr);
						}
					}
					else
					{
						// create header rec,
						size_t new_header_ptr = db_header_create(db,
							buildingpath.data,
							get_mtime(buildingpath.data), 1);
						verpv(new_header_ptr);
						// add header to parent dir
						db_directory_header_add(db, parent_src_ptr,
							new_header_ptr);
						// create dep rec
						size_t new_dep_ptr = db_dependency_create(db,
							src_ptr, new_header_ptr);
						verpv(new_dep_ptr);
					}
				}
				array_clear(&buildingpath);
			}
			HERE;
			dprintf(mfd, "\n");
			// exit(1) if CPP had error
			{
				int wstatus;
				waitpid(child, &wstatus, 0);
				if(WIFEXITED(wstatus))
				{
					HERE;
					int code = WEXITSTATUS(wstatus);
					verpv(code);
					if(code != 0)
					{
						fileavl_close(db);
						verpvs(getenv("EDITOR"));
						char* editor = getenv("EDITOR") ?: "gedit";
						execlp(editor, editor, src_path, NULL);
						perror("execl");
						abort();
					}
				}
			}
			HERE;
			// go through dep list:
			for(int i = 0;i < entries.n;i++)
			{
				struct entry* ele = arrayp_index(entries, i);
				verpv(ele);
				verpv(ele->header_ptr);
				verpvc(ele->should_remove ? 'y' : 'n');
				// removing if flagged to remove
				if(ele->should_remove)
				{
					#if 0
					char hdr_path[PATH_MAX];
					db_super_get_path(db, ele->header_ptr, hdr_path);
					verpvs(hdr_path);
					CHECK;
					#endif
					// remove dep rec, and reverse dep rec
					db_dependency_destory(db, ele->dependency_ptr);
					// dec header's refcount
					db_header_dec_refcount(db, ele->header_ptr);
					// add header to process queue
					db_reprocess_hdr_append(db, ele->header_ptr);
				}
			}
			// set queue ptr to 0
			db_source_set_queue_ptr(db, src_ptr, 0);
			close(mfd), close(pfd[1]);
			array_delete(&entries);
			EXIT;
			// remove source record from queue (done by db_reprocess_src_foreach)
		}
		callback;
	}));
	for(size_t i = 0;i < arch_flags_n;i++)
	{
		free(array_index(cpp_command, arch_flags_index + i, char*));
	}
	array_delete(&buildingpath);
	array_delete(&cpp_command);
	EXIT;
}




















