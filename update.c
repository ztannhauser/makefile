#include <stdlib.h>
#include <linux/limits.h>
#include <assert.h>
#include <stdio.h>

#define DEBUG 1
#include "libdebug.h"
#include "libarray.h"

#include "db/tree_ids.h"
#include "db/super/get_path.h"
#include "db/super/find.h"
#include "db/source/destory.h"
#include "db/source/get_parent.h"
#include "db/source/set_mtime.h"
#include "db/source/get_mtime.h"
#include "db/source/set_queue_ptr.h"
#include "db/source/get_queue_ptr.h"
#include "db/header/get_mtime.h"
#include "db/header/set_mtime.h"
#include "db/header/dec_refcount.h"
#include "db/dependency/foreach_source_of_header.h"
#include "db/dependency/foreach_header_of_source.h"
#include "db/dependency/destory.h"
#include "db/reprocess_src/append.h"
#include "db/reprocess_src/remove.h"
#include "db/reprocess_hdr/append.h"
#include "db/reprocess_dir/append.h"
#include "db/directory/dec_refcount.h"
#include "db/directory/subdir/foreach.h"
#include "db/directory/header/foreach.h"
#include "db/directory/source/foreach.h"
#include "db/directory/source/remove.h"

#include "./get_mtime.h"

void update(struct db* db)
{
	ENTER;
	struct bundle
	{
		size_t dir_src_ptr;
		size_t src_ptr;
	};
	struct array sources_to_remove = new_array(struct bundle);
	size_t root = db_super_find(db, ti_directories, ".");
	verpv(root);
	assert(root);
	{
		char path[PATH_MAX];
		void go(size_t dir)
		{
			db_directory_subdir_foreach(db, dir, go);
			// check source files:
			db_directory_source_foreach(db, dir, ({
				void callback(size_t rec_ptr, size_t src_ptr)
				{
					ENTER;
					verpv(src_ptr);
					db_super_get_path(db, src_ptr, path);
					verpvs(path);
					printf("Checking source file \"%s\"\n", path);
					time_t mtime = get_mtime(path);
					verpv(mtime);
					// if they exist:
					if(mtime)
					{
						time_t record_mtime = db_source_get_mtime(db, src_ptr);
						verpv(record_mtime);
						// if they've been modified:
						if(mtime > record_mtime)
						{
							size_t qptr = db_source_get_queue_ptr(db, src_ptr);
							verpv(qptr);
							// if source file is not already on queue
								// (using queue ptr):
							if(!qptr)
							{
								// add source file record to the reconsider queue,
								db_reprocess_src_append(db, p_modified, src_ptr);
							}
							// set new mtime
							db_source_set_mtime(db, src_ptr, mtime);
							//printf("Added source file \"%s\" to queue (mod)\n",
							//	path);
						}
					}
					// if they don't exist:
					else
					{
						printf("\tit no longer exists,\n");
						// note to remove after
						struct bundle b = 
						{
							.dir_src_ptr = rec_ptr,
							.src_ptr = src_ptr
						};
						array_push_n(&sources_to_remove, &b);
					}
					EXIT;
				}
				callback;
			}));
			// check header files:
			db_directory_header_foreach(db, dir, ({
				void callback(size_t hdr_ptr)
				{
					ENTER;
					verpv(hdr_ptr);
					db_super_get_path(db, hdr_ptr, path);
					verpvs(path);
					printf("Checking header file \"%s\"\n", path);
					time_t mtime = get_mtime(path);
					verpv(mtime);
					time_t record_mtime = db_header_get_mtime(db, hdr_ptr);
					verpv(record_mtime);
					// if they don't exist or modified:
					if(mtime == 0 || mtime > record_mtime)
					{
						// printf("Added all source files of header \"%s\" to "
						// "queue\n",
						//	path);
						// add includees to the reconsider queue
						db_dependency_foreach_source_of_header(db, hdr_ptr, ({
							void callback(size_t src_ptr)
							{
								ENTER;
								size_t rec =
									db_reprocess_src_append(db, p_modified, src_ptr);
								if(rec)
								{
									db_source_set_queue_ptr(db, src_ptr, rec);
								}
								EXIT;
							}
							callback;
						}));
						// record header's new mtime
						db_header_set_mtime(db, hdr_ptr, mtime);
					}
					EXIT;
				}
				callback;
			}));
		}
		go(root);
		// remove each source file record
		{
			verpv(sources_to_remove.n);
			for(size_t i = 0, n = sources_to_remove.n;i < n;i++)
			{
				printf("Removing source %i of %i:\n", i, n);
				struct bundle* ele = arrayp_index(sources_to_remove, i);
				verpv(ele);
				// remove source file references from dependencies
				db_dependency_foreach_header_of_source(db, ele->src_ptr, ({
					void callback(size_t dep_ptr, size_t hdr_ptr)
					{
						ENTER;
						db_dependency_destory(db, dep_ptr);
						db_header_dec_refcount(db, hdr_ptr);
						db_reprocess_hdr_append(db, hdr_ptr);
						EXIT;
					}
					callback;
				}));
				// remove source file reference from parent dir
				{
					size_t parent_ptr = db_source_get_parent(db, ele->src_ptr);
					db_directory_source_remove(db, ele->dir_src_ptr);
					db_directory_dec_refcount(db, parent_ptr);
					db_reprocess_dir_append(db, parent_ptr);
				}
				// possibly remove from reconsider queue // could happen
				{
					size_t qptr = db_source_get_queue_ptr(db, ele->src_ptr);
					if(qptr)
					{
						db_reprocess_src_remove(db, qptr);
					}
				}
				// delete source file record
				db_source_destory(db, ele->src_ptr);
			}
		}
		array_delete(&sources_to_remove);
	}
	EXIT;
}

























